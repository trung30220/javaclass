package com.breakaloop.scheduler;

public class Constant {
    public static final String SCHEDULER_PACKAGE_SERVICE = "com.breakaloop.scheduler.service";
    public static final String SCHEDULER_PACKAGE_CONFIG = "com.breakaloop.scheduler.config";
}
