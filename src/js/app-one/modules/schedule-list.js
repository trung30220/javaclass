import React, { Component } from 'react';
import { Segment, Form, Dropdown } from 'semantic-ui-react'
import { getQueryString, updateQueryString, axiosGet, axiosPost, copyToClipboard } from '../../utils'
import { MyContext } from '../lazy'
import { matchesToTable } from '../utils'
import UtilsChosePitchChild from './utils-chose-pitch-child'

export default class ScheduleList extends Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.getScheduleList = this.getScheduleList.bind(this);
        this.getScheduleListByInput = this.getScheduleListByInput.bind(this);
        this.getScheduleListByRange = this.getScheduleListByRange.bind(this);
    }

    handleChange(event, { name, value }, storage) {
        storage.scheduleList = {...storage.scheduleList, [name]: event.target.value};
        storage.updateStorage(storage);
    }

    getScheduleListByInput(storage) {
        let today = new Date();
        axiosGet('/api/san-ball-7/pitch/by-day?pitchId='+storage.scheduleList.pitchId
                +'&date='+storage.scheduleList.date
                +'&status=', null,
            function(response) {
                storage.scheduleList.response = response.data;
                storage.scheduleList.responseByDay = response.data;
                storage.updateStorage(storage);
            }, function(response) {
                console.log(response);
            });
    }

    getScheduleList(storage) {
        let today = new Date();
        axiosGet('/api/san-ball-7/pitch/by-day?pitchId='+storage.scheduleList.pitchId
                +'&date='+today.getFullYear()+'-'+(today.getMonth())+'-'+today.getDate()
                +'&status=', null,
            function(response) {
                storage.scheduleList.response = response.data;
                storage.scheduleList.responseByDay = response.data;
                storage.updateStorage(storage);
            }, function(response) {
                console.log(response);
            });
    }

    getScheduleListByRange(storage) {
        let today = new Date();
        axiosGet('/api/san-ball-7/pitch/by-day?pitchId='+storage.scheduleList.pitchId
                +'&date='+today.getFullYear()+'-'+today.getMonth()+'-'+today.getDate()
                +'&status=', null,
            function(response) {
                storage.scheduleList.response = response.data;
                storage.updateStorage(storage);
            }, function(response) {
                console.log(response);
            });
    }

    render() {
        return (
            <MyContext.Consumer>
                {({storage}) => (
                    <div>
                        <Form>
                            <UtilsChosePitchChild localStorage={storage.scheduleList}/>
                            <Form.Group>
                                <Form.Field>
                                <Form.Input label='date' name='date'
                                    value={storage.scheduleList.date}
                                    onChange={(event, data) =>
                                        this.handleChange(event, data, storage)} />
                                </Form.Field>
                            </Form.Group>
                            <Form.Group>
                                <Form.Field>
                                <Form.Button content='Get Schedule Today'
                                    onClick={() => this.getScheduleList(storage)} />
                                </Form.Field>
                                <Form.Field>
                                <Form.Button content='Get Schedule By Input'
                                    onClick={() => this.getScheduleListByInput(storage)} />
                                </Form.Field>
                                <Form.Field>
                                <Form.Button content='Get Schedule List By Range'
                                    onClick={() => this.getScheduleListByRange(storage)} />
                                </Form.Field>
                                <Form.Field>
                                <Form.Button content='Copy'
                                    onClick={() => copyToClipboard(JSON.stringify(storage.scheduleList.response))} />
                                </Form.Field>
                            </Form.Group>
                        </Form>
                        <strong>response:</strong>
                        <Segment style={{ overflow: 'auto', maxHeight: '27em' }}>
                            <pre> {JSON.stringify(storage.scheduleList.response, null, 2)} </pre>
                        </Segment>
                        <Segment style={{ overflow: 'auto', maxHeight: '27em' }}>
                            { matchesToTable(storage.scheduleList.responseByDay) }
                        </Segment>
                    </div>
                )}
            </MyContext.Consumer>
        )
    }
}
