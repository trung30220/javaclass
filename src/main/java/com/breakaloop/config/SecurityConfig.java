package com.breakaloop.config;

import com.breakaloop.filter.AuthFilter;
import com.breakaloop.filter.LimitAccessFilter;
import com.breakaloop.filter.TokenFilter;
import com.breakaloop.service.impl.auth.SocialConnectionSignup;
import com.breakaloop.service.impl.auth.SocialSignInAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.web.context.SecurityContextPersistenceFilter;
import org.springframework.security.web.context.request.async.WebAsyncManagerIntegrationFilter;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.connect.jdbc.JdbcUsersConnectionRepository;
import org.springframework.social.connect.support.ConnectionFactoryRegistry;
import org.springframework.social.connect.web.ProviderSignInController;
import org.springframework.social.facebook.connect.FacebookConnectionFactory;
import org.springframework.social.google.connect.GoogleConnectionFactory;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Value("${spring.social.facebook.appId}")
    private String springSocialFacebookAppId;
    @Value("${spring.social.facebook.appSecret}")
    private String springSocialFacebookAppSecret;
    @Value("${spring.social.google.appId}")
    private String springSocialGoogleAppId;
    @Value("${spring.social.google.appSecret}")
    private String springSocialGoogleAppSecret;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private ConnectionFactoryLocator connectionFactoryLocator;

    @Autowired
    private SocialConnectionSignup facebookConnectionSignup;

    @Autowired
    private DataSource dataSource;

    private UsersConnectionRepository usersConnectionRepository;

    @Override
    protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
    }

//        @Bean
//        // @Primary
//        public ProviderSignInController providerSignInController() {
//                ((InMemoryUsersConnectionRepository) usersConnectionRepository).setConnectionSignUp(facebookConnectionSignup);
//                return new ProviderSignInController(connectionFactoryLocator, usersConnectionRepository, new SocialSignInAdapter());
//        }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        // /api/** = /api + /api/
        // /api/* = /api/

        // well-known for certificate (let's encrypted)
        http
                //.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.NEVER).and()
                .csrf().disable()
                .addFilterBefore(new AuthFilter(), WebAsyncManagerIntegrationFilter.class)
                .addFilterAfter(new LimitAccessFilter(), AuthFilter.class)
                .addFilterAfter(new TokenFilter(), SecurityContextPersistenceFilter.class);
//                .authorizeRequests()
//                .antMatchers("/.well-known/**", "/login*", "/signin/**", "/signup/**",
//                        "/api/**", // authenticated by filter
//                        "/resources/**").permitAll()
//                .anyRequest().authenticated()
//                .and()
//                .formLogin().loginPage("/login")
//                .permitAll()
//                .and()
//                .logout();

//                .and()
//                .rememberMe()
//                .rememberMeServices(rememberMeServices());
    }

//    @Bean
//    RememberMeServices rememberMeServices() {
//        SpringSessionRememberMeServices rememberMeServices =
//                new SpringSessionRememberMeServices();
//        // optionally customize
//        rememberMeServices.setAlwaysRemember(true);
//        return rememberMeServices;
//    }

    @Bean
    public ConnectionFactoryLocator connectionFactoryLocator() {
        ConnectionFactoryRegistry registry = new ConnectionFactoryRegistry();
        registry.addConnectionFactory(facebookConnectionFactory());
        registry.addConnectionFactory(googleConnectionFactory());
        return registry;
    }

    @Bean
    public FacebookConnectionFactory facebookConnectionFactory() {
        String appId = springSocialFacebookAppId;
        String appSecret = springSocialFacebookAppSecret;

        FacebookConnectionFactory facebookConnectionFactory = new FacebookConnectionFactory(appId, appSecret);
        facebookConnectionFactory.setScope("public_profile,account");
        return facebookConnectionFactory;
    }

    @Bean
    public ProviderSignInController providerSignInController() {
        usersConnectionRepository = new JdbcUsersConnectionRepository(dataSource, connectionFactoryLocator(), Encryptors.noOpText());
        ((JdbcUsersConnectionRepository) usersConnectionRepository).setConnectionSignUp(facebookConnectionSignup);
        return new ProviderSignInController(
                connectionFactoryLocator,
                usersConnectionRepository,
                new SocialSignInAdapter());
    }

    // google
    // quang cao
    // to do
    // diary
    // english game

    @Bean
    public GoogleConnectionFactory googleConnectionFactory() {
        GoogleConnectionFactory googleConnectionFactory =
                new GoogleConnectionFactory(springSocialGoogleAppId, springSocialGoogleAppSecret);
        googleConnectionFactory.setScope("profile");
        return googleConnectionFactory;
    }
}