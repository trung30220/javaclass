import React, { Component } from 'react';
import { Segment, Form } from 'semantic-ui-react'
import { getQueryString, updateQueryString, axiosGet, axiosPost, copyToClipboard } from '../../utils'
import { MyContext } from '../lazy'

export default class TeamListManager extends Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.list = this.list.bind(this);
    }

    handleChange(event, { name, value }, storage) {
        storage.teamList = {...storage.teamList, [name]: event.target.value};
        storage.updateStorage(storage);
    }

    list(storage) {
        axiosGet('/api/san-ball-7/team/list?page='
                    + storage.teamList.page
                    + '&size=' + storage.teamList.size, null,
            function(response){
                storage.teamList.response = response.data;
                storage.updateStorage(storage);
            }, function(response) {
                console.log(response);
            });
    }

    render() {
        return (
            <MyContext.Consumer>
                {({storage}) => (
                    <div>
                        <Form>
                            <Form.Group>
                                <Form.Field width={8}>
                                <Form.Input label='page' name='page'
                                    value={storage.teamList.page}
                                    onChange={(event, data) =>
                                        this.handleChange(event, data, storage)} />
                                </Form.Field>
                                <Form.Field width={8}>
                                <Form.Input label='size' name='size'
                                    value={storage.teamList.size}
                                    onChange={(event, data) =>
                                        this.handleChange(event, data, storage)} />
                                </Form.Field>
                            </Form.Group>
                            <Form.Group>
                                <Form.Field>
                                <Form.Button content='Get Team List'
                                    onClick={() => this.list(storage)} />
                                </Form.Field>
                                <Form.Field>
                                <Form.Button content='Copy'
                                    onClick={() => copyToClipboard(JSON.stringify(storage.teamList.response))} />
                                </Form.Field>
                            </Form.Group>
                        </Form>
                        <strong>response:</strong>
                        <Segment style={{ overflow: 'auto', maxHeight: '27em' }}>
                            <pre> {JSON.stringify(storage.teamList.response, null, 2)} </pre>
                        </Segment>
                    </div>
                )}
            </MyContext.Consumer>
        )
    }
}
