package com.breakaloop.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Embeddable
public class UserConnectionIdentity implements Serializable {
    private static final long serialVersionUID = 8043083817494685551L;

    @NotNull
    @Column(name = "userid")
    String userId;
    @NotNull
    @Column(name = "providerid")
    String providerId;
    @NotNull
    @Column(name = "provideruserid")
    String providerUserId;

    public UserConnectionIdentity() {

    }

    public UserConnectionIdentity(String userId, String providerId, String providerUserId) {
        this.userId = userId;
        this.providerId = providerId;
        this.providerUserId = providerUserId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public String getProviderUserId() {
        return providerUserId;
    }

    public void setProviderUserId(String providerUserId) {
        this.providerUserId = providerUserId;
    }
}
