/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.breakaloop.helper;

import com.breakaloop.constant.Constants;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.Collection;
import java.util.Map;

/**
 *
 * @author Carlos
 */
public class JsonHelper {
    
    private final ObjectMapper MAPPER = new ObjectMapper();

  
     /*
     * Helper methods
     */
    public Map<String, Object> getResponseEntity(final Response response) throws JsonParseException,
            JsonMappingException, IOException {
        return MAPPER.readValue(response.readEntity(String.class),
                new TypeReference<Map<String, Object>>() {
                });
    }

    public Map<String, Object> getJsonMap(final String response) throws JsonParseException,
            JsonMappingException, IOException {
        return MAPPER.readValue(response,
                new TypeReference<Map<String, Object>>() {
                });
    }

    static ExclusionStrategy exclusionStrategy = new ExclusionStrategy() {
        @Override
        public boolean shouldSkipField(FieldAttributes fieldAttributes) {
            Collection<Annotation> annotations = fieldAttributes.getAnnotations();
            for (Annotation annotation : annotations) {
                if (annotation instanceof JsonIgnore) {
                    return true;
                }
            }
            return false;
        }

        @Override
        public boolean shouldSkipClass(Class<?> aClass) {
            return false;
        }
    };

    public static Gson getGson(boolean isPretty) {
        if (isPretty) {
            return new GsonBuilder().setExclusionStrategies(exclusionStrategy).setPrettyPrinting()
                    .setDateFormat(Constants.FULL_DATE_TIME_FORMAT).create();
        } else {
            return new GsonBuilder().setExclusionStrategies(exclusionStrategy)
                    .setDateFormat(Constants.FULL_DATE_TIME_FORMAT).create();
        }
    }
}
