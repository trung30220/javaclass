package com.breakaloop.app.quartzjob;

import com.breakaloop.app.quartzjob.entity.JobGeneric;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

import static com.breakaloop.constant.Constants.FULL_DATE_TIME_FORMAT;
import static com.breakaloop.app.quartzjob.Constants.CRON;
import static com.breakaloop.app.quartzjob.Constants.FIRE_TIME;
import static java.time.ZoneId.systemDefault;
import static org.quartz.CronExpression.isValidExpression;
import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.JobKey.jobKey;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;
import static org.springframework.util.StringUtils.isEmpty;

@Service
public class JobGenericServiceImpl implements JobGenericService {

    private static final Logger logger = LoggerFactory.getLogger(JobGenericServiceImpl.class);

    @Autowired
    private JobGenericRepository jobGenericRepository;

    @Autowired
    private Scheduler scheduler;

    @Override
    public JobGeneric getJob(String group, String name) {
        return jobGenericRepository.findByGroupAndName(group, name);
    }

    private Trigger buildTrigger(String group, String name, String cron, Date fireTime) {
        // fixme add .startNow()
        if (!isEmpty(cron)) {
            if (!isValidExpression(cron))
                throw new IllegalArgumentException("Provided expression " + cron + " is not a valid cron expression");
            return newTrigger().withIdentity(name, group).withSchedule(cronSchedule(cron)
                    .withMisfireHandlingInstructionFireAndProceed().inTimeZone(TimeZone.getTimeZone(systemDefault())))
                    .usingJobData(CRON, cron).build();
        } else if (!isEmpty(fireTime)) {
            JobDataMap jobDataMap = new JobDataMap();
            jobDataMap.put(FIRE_TIME, fireTime);
            return newTrigger().withIdentity(name, group)
                    .withSchedule(simpleSchedule().withMisfireHandlingInstructionNextWithExistingCount())
                    .startAt(fireTime).usingJobData(jobDataMap).build();//.startNow().build();
        }
        throw new IllegalStateException("unsupported trigger descriptor " + this);
    }

    @Override
    public JobGeneric createJobAt(String group, String name, Map<String, Object> data, LocalDateTime fireTime, ZoneId zoneId) {
        return createJob(group, name, data, null, fireTime, zoneId);
    }

    @Override
    public JobGeneric createCronJob(String group, String name, Map<String, Object> data, String cronExp) {
        return createJob(group, name, data, cronExp, null, null);
    }

    private JobGeneric createJob(String group, String name, Map<String, Object> data, String cronExp, LocalDateTime localFireTime, ZoneId zoneId) {
        if (data == null) data = new HashMap<>(); // set default

        JobGeneric jobGeneric = new JobGeneric();
        jobGeneric.setDataMap(data);
        jobGeneric.setName(name);
        jobGeneric.setGroup(group);
        jobGeneric.setCronExpression(cronExp);
        try {
            jobGeneric.setLocalFireTime(localFireTime, zoneId);
        } catch (ParseException e) {
            logger.debug("Job {}", group, e);
        }

        Trigger trigger = buildTrigger(group, name, cronExp,
                localFireTime!=null?Date.from(localFireTime.atZone(zoneId).toInstant()):null);
        trigger.getJobDataMap().putAll(data);
        JobDataMap jobDataMap = new JobDataMap(data);
        JobDetail jobDetail = null;

        Assert.notNull(group, "Property 'jobClass' is required");

        try {
            jobDetail = newJob(Class.forName(group).asSubclass(Job.class)).withIdentity(name, group).usingJobData(jobDataMap).build();
        } catch (ClassNotFoundException e) {
            logger.error("Job class not found", e);
            return null;
        }

        if (jobDetail != null) {
            Optional<JobDescriptor> job = findJob(group, name);
            JobGeneric dbJogGeneric = jobGenericRepository.findByGroupAndName(group, name);
            if (job.isPresent()) {
                logger.debug("Job existed {} {}", jobDetail.getKey(), dbJogGeneric);
                if (dbJogGeneric != null) {
                    logger.debug("Job existed at DB {} {} {}", dbJogGeneric.getId(), dbJogGeneric.getData(), dbJogGeneric.getFireTime());
                    jobGenericRepository.delete(dbJogGeneric);
                    dbJogGeneric = null;
                }
                deleteJob(group, name);
            }
            try {
                scheduler.scheduleJob(jobDetail, trigger);
                logger.info("Job with key - {} saved successfully", jobDetail.getKey());
            } catch (SchedulerException e) {
                logger.error("Could not save job with key - {} due to error - {}", jobDetail.getKey(),
                        e.getLocalizedMessage());
//                throw new IllegalArgumentException(e.getLocalizedMessage());
            }

            if (dbJogGeneric == null) {
                logger.info("TestTestTest: "+jobGeneric.getId().toString().length() + " " + jobGeneric.getId().toString());
                return jobGenericRepository.save(jobGeneric);
            } else {
                logger.debug("Job existed at DB {} {} {}", dbJogGeneric.getId(), dbJogGeneric.getData(), dbJogGeneric.getFireTime());
            }
        } else {
            logger.info("JobExpireEntityServiceImpl: group not found {}", group);
        }
        return null;
    }

    @Override
    public void deleteJob(byte[] id) {
        jobGenericRepository.deleteById(id);
    }

    @Override
    //@Transactional(readOnly = true)
    public Optional<JobDescriptor> findJob(String group, String name) {
        try {
            JobDetail jobDetail = scheduler.getJobDetail(jobKey(name, group));
            if (Objects.nonNull(jobDetail))
                return Optional
                        .of(JobDescriptor.buildDescriptor(jobDetail, scheduler.getTriggersOfJob(jobKey(name, group))));
        } catch (SchedulerException e) {
            logger.error("Could not find job with key - {} - {} due to error - {}", group, name, e.getLocalizedMessage());
        }
        logger.warn("Could not find job with key - {} - {}", group, name);
        return Optional.empty();
    }

    @Override
    public void updateJob(String group, String name, Map<String, Object> data) {
        try {
            JobDetail oldJobDetail = scheduler.getJobDetail(jobKey(name, group));
            if (Objects.nonNull(oldJobDetail)) {
                JobDataMap jobDataMap = oldJobDetail.getJobDataMap();
                jobDataMap.putAll(data);
                JobBuilder jb = oldJobDetail.getJobBuilder();
                JobDetail newJobDetail = jb.usingJobData(jobDataMap).storeDurably().build();
                scheduler.addJob(newJobDetail, true);
                logger.info("Updated job with key - {}", newJobDetail.getKey());
                return;
            }
            logger.warn("Could not find job with key - {} - {} to update", group, name);
        } catch (SchedulerException e) {
            logger.error("Could not find job with key - {} - {} to update due to error - {}", group, name,
                    e.getLocalizedMessage());
        }
    }

    @Override
    public void deleteJob(String group, String name) {
        try {
            scheduler.deleteJob(jobKey(name, group));
            logger.info("Deleted job with key - {} - {}", group, name);
        } catch (SchedulerException e) {
            logger.error("Could not delete job with key - {} - {} due to error - {}", group, name, e.getLocalizedMessage());
        }
    }

    @Override
    public void pauseJob(String group, String name) {
        try {
            scheduler.pauseJob(jobKey(name, group));
            logger.info("Paused job with key - {} - {}", group, name);
        } catch (SchedulerException e) {
            logger.error("Could not pause job with key - {} - {} due to error - {}", group, name, e.getLocalizedMessage());
        }
    }

    @Override
    public void resumeJob(String group, String name) {
        try {
            scheduler.resumeJob(jobKey(name, group));
            logger.info("Resumed job with key - {} - {}", group, name);
        } catch (SchedulerException e) {
            logger.error("Could not resume job with key - {} - {} due to error - {}", group, name, e.getLocalizedMessage());
        }
    }

	@Override
	public List<JobGeneric> findAll() {
		return jobGenericRepository.findAllNotExpired();
	}

	@Override
    public void initGenericJobs() {
        Date now = new Date();
        List<JobGeneric> jobGenerics = jobGenericRepository.findAll();
        try {
            if (CollectionUtils.isEmpty(jobGenerics)) {
                return;
            }
            jobGenerics.forEach(jobGeneric -> {
                logger.info("Creating job {} {} (id:{})... {}", jobGeneric.getGroup(), jobGeneric.getName(), jobGeneric.getId(), jobGeneric.getActive());
                if (jobGeneric.getActive()) {
                    if (jobGeneric.getFireTime() != null) {
                        String fireTimeStr = jobGeneric.getFireTime();

                        LocalDateTime fireTime = null;
                        ZoneId zoneId = ZoneId.systemDefault();
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(FULL_DATE_TIME_FORMAT);

                        try {
                            Date fireTimeDate = simpleDateFormat.parse(fireTimeStr);
                            fireTime = fireTimeDate.toInstant().atZone(zoneId).toLocalDateTime();
                        } catch (ParseException e) {
                            logger.debug("OnStartUp parse error {} {}", fireTimeStr, jobGeneric.getId());
                        }

                        LocalDateTime localDateTime = now.toInstant().atZone(zoneId).toLocalDateTime();
                        if (fireTime != null && fireTime.compareTo(localDateTime) > 0) {
                            createJobAt(jobGeneric.getGroup(), jobGeneric.getName(), jobGeneric.getDataMap(), fireTime, zoneId);
                        } else {
                            logger.debug("Disable job {} {} cause {} <= {}", jobGeneric.getGroup(), jobGeneric.getId(), jobGeneric.getLocalFireTime(), localDateTime);
                        }
                    } else {
                        createCronJob(jobGeneric.getGroup(), jobGeneric.getName(), jobGeneric.getDataMap(), jobGeneric.getCronExpression());
                    }
                }
            });
        } catch (Exception e) {
            logger.error("Creating job {} error", e);
        }
    }
}
