import React, { Component } from 'react'
import { Header, Segment, Container, Form, Tab } from 'semantic-ui-react'
import { axiosGet, axiosPost } from '../utils'
import UserManager from './modules/user'
import TeamManager from './modules/team'
import Service from './modules/service'
import Pitch from './modules/pitch'
import Schedule from './modules/schedule'
import Upload from './modules/upload'

export const MyContext = React.createContext('san-ball-7');

export default class LazyAppOne extends Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.updateStorage = this.updateStorage.bind(this);
        this.loadData = this.loadData.bind(this);
        this.state = {
            preLoadData: true,
            name: '',
            email: '',
            submittedName: '',
            submittedEmail: '',
            storage: {
                updateStorage: this.updateStorage,
                upload: {
                    response: {},
                },
                pitchList: {
                    page: 0,
                    size: 10,
                    response: {},
                },
                pitchAllService: {
                    pitchId: '',
                    response: {},
                },
                teamAddMember: {
                    teamId: null,
                    userIds: [],
                    response: {},
                },
                scheduleEditAll: {
                    pitchChildId: '',
                    response: {},
                },
                scheduleList: {
                    date: '2019-04-13',
                    pitchId: null,
                    pitchChildId: null,
                    pitchChildSet: [],
                    page: 0,
                    size: 10,
                    response: {},
                    responseByDay: {},
                },
                scheduleCreate: {
                    request: {
                        booking: true,
                        valid: true,
                        monday: true,
                        tuesday: true,
                        wednesday: true,
                        thursday: true,
                        friday: true,
                        saturday: true,
                        sunday: true,
                        fromYear: '',
                        fromMonth: '',
                        fromDay: '',
                        fromHour: '',
                        fromMinute: '',
                        toYear: '',
                        toMonth: '',
                        toDay: '',
                        toHour: '',
                        toMinute: '',
                        teamId: '',
                    },
                    response: {},
                },
                serviceList: {
                    page: 0,
                    size: 10,
                    response: {},
                },
                teamList: {
                    page: 0,
                    size: 10,
                    response: {},
                },
                teamCreate: {
                    request: {
                        name: '',
                    },
                    response: {},
                },
                teamFind: {
                    teamId: '',
                    name: '',
                    response: {},
                },
                userList: {
                    page: 0,
                    size: 10,
                    response: {},
                },
                currentUser: { // UserManager.user-current
                    request: {
                        account: '',
                        password: '',
                        pitchChildNum: 0,
                        pitchName: '',
                    }
                },
                serviceCreate: {
                    pitchId: null,
                    request: {
                        name: '',
                        description: '',
                    },
                    response: {},
                },
            },
        }
    }

    handleChange(event, { name, value }) {
        this.setState({[name]: event.target.value});
    }

    handleSubmit() {
        const { name, email } = this.state
        this.setState({ submittedName: name, submittedEmail: email })
    }

    handleChangeUser(event) {
        this.setState({user: event.target.value});
    }
    handleChangeMessage(event) {
        this.setState({message: event.target.value});
    }

    updateStorage(storage) {
        this.setState({storage: storage});
    }

    loadData(storage) {
        axiosGet('/api/san-ball-7/user/list?page=0&size=1000', null,
            function(response){
                storage.userList.response = response.data;
                storage.updateStorage(storage);
            }, function(response) {
                console.log(response);
            });
        axiosGet('/api/san-ball-7/team/list?page=0&size=1000', null,
            function(response){
                storage.teamList.response = response.data;
                storage.updateStorage(storage);
            }, function(response) {
                console.log(response);
            });
        axiosGet('/api/san-ball-7/pitch/all?page=0&size=1000', null,
            function(response){
                storage.pitchList.response = response.data;
                storage.updateStorage(storage);
            }, function(response) {
                console.log(response);
            });
    }

    render() {
        if (this.state.preLoadData) {
            this.state.preLoadData = false;
            this.loadData(this.state.storage);
            return (<div/>);
        }
        console.log('test ' + this.state.preLoadData)

        const { name, email, submittedName, submittedEmail } = this.state

        const panes = [
            { menuItem: 'User', render: () => <UserManager/> },
            { menuItem: 'Team', render: () => <TeamManager/> },
            { menuItem: 'Service', render: () => <Service/> },
            { menuItem: 'Pitch', render: () => <Pitch/> },
            { menuItem: 'Schedule', render: () => <Schedule/> },
            { menuItem: 'Upload', render: () => <Upload/> },
        ]

        return (
            <MyContext.Provider value={{storage: this.state.storage, updateStorage: this.updateStorage}}>
                <Segment basic>
                    <Header>Command</Header>
                    <Segment.Group>
                        <Segment><button onClick={() => this.props.closeApp()}>Exit</button></Segment>
                        <Segment><button onClick={() => this.loadData(this.state.storage)}>Load Data</button></Segment>
                    </Segment.Group>
                    <Tab panes={panes} />
                </Segment>

                <Container>
                </Container>
            </MyContext.Provider>
        )
    }
}
