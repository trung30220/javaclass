const axios = require('axios');

export var GlobalStorage = (function () {
    const DEFAULT = "default";
    var instance;

    function createInstance() {
        var object = {[DEFAULT]:{}};//{default:{}};//new Object("I am the instance");
        return object;
    }

    return {
        getDefaultName: function() {
            return DEFAULT;
        },
        getInstance: function (storageName) {
            if (!instance) {
                instance = createInstance();
            }
            if (typeof storageName == 'undefined') {
                return instance[DEFAULT];//.default;
            } else {
                instance[storageName] = {};
                console.log(instance);
                return instance[storageName];
            }
        }
    };
})();

//function run() {
//
//    var instance1 = Singleton.getInstance();
//    var instance2 = Singleton.getInstance();
//
//    alert("Same instance? " + (instance1 === instance2));
//}

//export function redirect(url) {
//    if (url.charAt(0) != '/') url = '/'+url
//    var saveData = JSON.parse(localStorage.saveData || null) || {};
//    if (saveData.token) {
//        let auth = '?token='+saveData.token+'&web=breakaloop.com';
////        console.log(location.origin);
////        console.log(location.origin == 'http://localhost:8080');
////        console.log(url+auth)
//        if (location.origin == 'http://localhost:8080') {
//            window.location.replace(url+auth)
//        } else if (location.protocol != 'https:') {
//            // TODO: use this instead return login page
//            //location.href = 'https:' + window.location.href.substring(window.location.protocol.length) + auth;
//            window.location.replace('/login')
//        } else {
//            window.location.replace(url+auth)
//        }
//    } else {
//        window.location.replace('/login')
//    }
//}

function preventMotion(event)
{
    window.scrollTo(0, 0);
    event.preventDefault();
    event.stopPropagation();
}
function preventControl(event) {
    if (event.ctrlKey == true) {
        event.preventDefault();
    }
}
function preventKey(event) {
    if (event.ctrlKey==true && (event.which == '61' || event.which == '107' || event.which == '173' || event.which == '109'  || event.which == '187'  || event.which == '189'  ) ) {
        event.preventDefault();
        // 107 Num Key  +
        //109 Num Key  -
        //173 Min Key  hyphen/underscor Hey
        // 61 Plus key  +/=
    }
}
export function disableInfinity() {
    document.body.style.overflow = '';
    document.body.style['overflow-x'] = '';
    document.body.style.position = '';
    window.removeEventListener("scroll", preventMotion, false);
    window.removeEventListener("touchmove", preventMotion, false);
    document.removeEventListener("mousewheel", preventControl);
    document.removeEventListener("DOMMouseScroll", preventControl);
    document.removeEventListener("keydown", preventKey);
    document.getElementById('infinity-canvas').style.display = 'none';//remove();
}
export function existInfinity() {
    return document.getElementById('infinity-canvas');
}
export function enableInfinity() {
    document.body.style.overflow = 'hidden';
    document.body.style['overflow-x'] = 'hidden';
    document.body.style.position = 'relative';
    window.addEventListener("scroll", preventMotion, false);
    window.addEventListener("touchmove", preventMotion, false);
    document.addEventListener("mousewheel", preventControl);
    document.addEventListener("DOMMouseScroll", preventControl);
    document.addEventListener("keydown", preventKey);
    document.getElementById('infinity-canvas').style.display = '';
}

export function axiosGet(url, params, handleSuccess, handleFailure) {
    var saveData = JSON.parse(localStorage.saveData || null) || {};
    axios.get(url, {
            params: params,
            headers:{
                'Content-Type': 'application/json; charset=utf-8',
                'Authorization': 'Bearer: ' + saveData.token,
            }})
        .then(handleSuccess)
        .catch(handleFailure);
}

export function loadObjectUrl(url) {
    return new Promise(function(resolve, reject) {
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.async = true;
        script.src = url;
        script.onload = resolve;
        script.onerror = reject;
        document.head.appendChild(script);
    })
}

export function axiosPost(url, body, handleSuccess, handleFailure) {
    var saveData = JSON.parse(localStorage.saveData || null) || {};
    axios.post(url, body, {
            //params: params,
            headers:{
                'Content-Type': 'application/json; charset=utf-8',
                'Authorization': 'Bearer: ' + saveData.token,
            }})
        .then(function(response){handleSuccess(response)})
        .catch(function(response){handleSuccess(response)});
}

export function axiosDelete(url, params, handleSuccess, handleFailure) {
    var saveData = JSON.parse(localStorage.saveData || null) || {};
    axios.delete(url, {
            params: params,
            headers:{
                'Content-Type': 'application/json; charset=utf-8',
                'Authorization': 'Bearer: ' + saveData.token,
            }})
        .then(handleSuccess)
        .catch(handleFailure);
}

export function updateQueryString(params) {
//        console.log(urlParams.has('post')); // true
//        console.log(urlParams.get('action')); // "edit"
//        console.log(urlParams.getAll('action')); // ["edit"]
//        console.log(urlParams.toString()); // "?post=1234&action=edit"
//        console.log(urlParams.append('active', '1')); // "?post=1234&action=edit&active=1"

    var urlParams = new URLSearchParams(window.location.search);
    for (let param in params) {
        urlParams.set(param, params[param])
    }
    //window.location.search = urlParams.toString(); -> cause load page
    var newRelativePathQuery = window.location.pathname + '?' + urlParams.toString();
    history.pushState(null, '', newRelativePathQuery);
}

export function getQueryString() {
    return new URLSearchParams(window.location.search);
}

// use cookie
export function redirect(url) {
    if (url.charAt(0) != '/') url = '/'+url
    var saveData = JSON.parse(localStorage.saveData || null) || {};
    if (saveData.token) {
        let cookie = getCookie(OOKIEMY_COOKIE)
        if (cookie.length > 0) {
        } else {
            setCookie(MY_COOKIE, saveData.token)
        }
//        console.log(document.getElementsByTagName("title")[0].innerHTML)
//        changeUrl(document.getElementsByTagName("title")[0].innerHTML + '^^', location.origin + url)
//        console.log(location.origin + url)
        window.location.replace(url)
    } else {
        window.location.replace('/login')
    }
}

function changeUrl(title, url) {
    if (typeof (history.pushState) != "undefined") {
        var obj = { Title: title, Url: url };
        history.pushState(obj, obj.Title, obj.Url);
    } else {
        alert("Browser does not support HTML5.");
    }
}

export function updateAuthCookie() {
    var saveData = JSON.parse(localStorage.saveData || null) || {};

    // OLD LOGIC - token in URL
    if (!saveData.token) {
        const first = 'token=';
        const second = '&web=breakaloop.com';
        let url = window.location.href;
        if (url.includes(first) && url.includes(second)) {
            let array1 = url.split(first);
            let array2 = array1[1].split(second);

            saveData.token = array2[0];
            saveData.time = new Date().getTime();
            localStorage.saveData = JSON.stringify(saveData);
            window.history.replaceState({}, document.title, "/" + "");
        }
        // refresh cookie
        setCookie(MY_COOKIE, saveData.token)
    }

    if (saveData.token) {
//        let cookie = getCookie(MY_COOKIE)
//        if (cookie.length > 0) {
//        } else {
//            // first time
//            setCookie(MY_COOKIE, saveData.token)
//        }
    } else {
        console.log(saveData.token);
//        window.location.replace('/login')
    }
}

export function removeAuthCookie() {
    _setCookie(MY_COOKIE, "", -1)
}

const MY_COOKIE = "MY_COOKIE";
function setCookie(cname, cvalue) {
    _setCookie(cname, cvalue, 1)
}
function _setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

export const copyToClipboard = str => {
  const el = document.createElement('textarea');
  el.value = str;
  el.setAttribute('readonly', '');
  el.style.position = 'absolute';
  el.style.left = '-9999px';
  document.body.appendChild(el);
  el.select();
  document.execCommand('copy');
  document.body.removeChild(el);
}