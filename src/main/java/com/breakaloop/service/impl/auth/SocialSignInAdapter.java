package com.breakaloop.service.impl.auth;

import com.breakaloop.entity.User;
import com.breakaloop.helper.AuthUtils;
import com.breakaloop.helper.Token;
import com.nimbusds.jose.JOSEException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.web.SignInAdapter;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.ServletWebRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

public class SocialSignInAdapter implements SignInAdapter {

    @Override
    public String signIn(String localUserId, Connection<?> connection, NativeWebRequest nativeWebRequest) {
        User user = new User();
        user.setProviderId(connection.getKey().getProviderId());
        user.setProviderUserId(connection.getKey().getProviderUserId());

        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(user, null, Arrays.asList(new SimpleGrantedAuthority("FACEBOOK_USER"))));

        //return null;
        String redirectUrl = "/";
        HttpServletRequest request
                = (HttpServletRequest) nativeWebRequest.getNativeRequest();
        String remoteHost = request.getRemoteHost();
        try {
            HttpServletResponse response = ((ServletWebRequest) nativeWebRequest).getResponse();
            Token token = AuthUtils.createToken(remoteHost, localUserId);
            System.out.println("TOKEN: " + token.getToken());
            response.addHeader("Authorization", "Bearer: " + token.getToken()); // unused
            if (request.isSecure()
                    || request.getRequestURL().toString().startsWith("http://localhost:8080/")
                    || request.getRequestURL().toString().startsWith("http://localhost:5000/")
                    || request.getRequestURL().toString().startsWith("http://breakaloop.us-east-2.elasticbeanstalk.com/")) {
                redirectUrl += "?token=" + token.getToken() + "&web=breakaloop.com";
            } else {
                redirectUrl += "?token=please-switch-to-https";
                System.out.println(redirectUrl);
                System.out.println(redirectUrl);
                System.out.println(redirectUrl);
            }
        } catch (JOSEException e) {
            e.printStackTrace();
        }
        return redirectUrl;
    }
}