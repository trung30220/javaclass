//package com.breakaloop.controller;
//
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//@Controller
//public class TestController {
//
//    @GetMapping("/profile/employees")
//    @ResponseBody
//    public String test2() {
//        String data = "{ \"alps\" : { \"version\" : \"1.0\", \"descriptors\" : [ { \"id\" : \"employee-representation\", \"href\" : \"http://localhost:8080/profile/employees\", \"descriptors\" : [ { \"name\" : \"firstName\", \"type\" : \"SEMANTIC\" }, { \"name\" : \"lastName\", \"type\" : \"SEMANTIC\" }, { \"name\" : \"description\", \"type\" : \"SEMANTIC\" } ] }, { \"id\" : \"get-employees\", \"name\" : \"employees\", \"type\" : \"SAFE\", \"rt\" : \"#employee-representation\" }, { \"id\" : \"create-employees\", \"name\" : \"employees\", \"type\" : \"UNSAFE\", \"rt\" : \"#employee-representation\" }, { \"id\" : \"update-employee\", \"name\" : \"employee\", \"type\" : \"IDEMPOTENT\", \"rt\" : \"#employee-representation\" }, { \"id\" : \"get-employee\", \"name\" : \"employee\", \"type\" : \"SAFE\", \"rt\" : \"#employee-representation\" }, { \"id\" : \"patch-employee\", \"name\" : \"employee\", \"type\" : \"UNSAFE\", \"rt\" : \"#employee-representation\" }, { \"id\" : \"delete-employee\", \"name\" : \"employee\", \"type\" : \"IDEMPOTENT\", \"rt\" : \"#employee-representation\" } ] } }";
//        return data;
//    }
//
//    @GetMapping("/employees")
//    @ResponseBody
//    public String test1() {
//        String data = "{" +
//                "  \"_embedded\" : {" +
//                "    \"employees\" : [ {" +
//                "      \"firstName\" : \"Frodo TRUNGNNNN\"," +
//                "      \"lastName\" : \"Baggins\"," +
//                "      \"description\" : \"ring bearer\"," +
//                "      \"_links\" : {" +
//                "        \"self\" : {" +
//                "          \"href\" : \"http://localhost:8080/employees/1\"" +
//                "        }," +
//                "        \"employee\" : {" +
//                "          \"href\" : \"http://localhost:8080/employees/1\"" +
//                "        }" +
//                "      }" +
//                "    } ]" +
//                "  }," +
//                "  \"_links\" : {" +
//                "    \"self\" : {" +
//                "      \"href\" : \"http://localhost:8080/employees\"" +
//                "    }," +
//                "    \"profile\" : {" +
//                "      \"href\" : \"http://localhost:8080/profile/employees\"" +
//                "    }" +
//                "  }" +
//                "}";
//        return data;
//    }
//}