package com.breakaloop.controller;

import com.breakaloop.dto.UserDto;
import com.breakaloop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

@RequestMapping("/angularjs-bootstrap")
@RestController
@ApiIgnore
public class UserController {
    @Autowired
    UserService userService;

    @GetMapping("/getUser/{userId}")
    public UserDto getUserById(@PathVariable Integer userId) {
        return userService.getUserById(userId);
    }
    @GetMapping("/getAllUsers")
    public List< UserDto > getAllUsers() {
        return userService.getAllUsers();
    }
    @RequestMapping(value = "/saveUser", method = RequestMethod.POST)
    public void saveUser(@RequestBody UserDto userDto) {
        userService.saveUser(userDto);
    }
}
