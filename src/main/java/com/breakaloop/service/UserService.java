package com.breakaloop.service;

import com.breakaloop.dto.UserDto;

import java.util.List;

public interface UserService {
    UserDto getUserById(Integer userId);
    void saveUser(UserDto userDto);
    List< UserDto > getAllUsers();
}

