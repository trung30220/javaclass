package com.breakaloop.repository;

import com.breakaloop.entity.UserConnection;
import com.breakaloop.entity.UserConnectionIdentity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserConnectionRepository extends JpaRepository<UserConnection, UserConnectionIdentity> {
    @Query("from UserConnection uc where uc.userConnectionIdentity.userId = :userId")
    UserConnection findById(@Param("userId") String userId);

    @Query("from UserConnection uc where uc.userConnectionIdentity.providerId = :providerId" +
            " and uc.userConnectionIdentity.providerUserId = :providerUserId")
    UserConnection findByProviderIdAndUserProviderId(
            @Param("providerId") String providerId,
            @Param("providerUserId") String providerUserId);
}
