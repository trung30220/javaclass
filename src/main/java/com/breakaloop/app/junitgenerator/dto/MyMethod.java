package com.breakaloop.app.junitgenerator.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MyMethod implements Comparable<MyMethod> {
    String identifier;
    String name;

    Class returnType;
    Object returnObject;

    List<MyBlock> myBlocks = new ArrayList<>();

    @Override
    public int compareTo(MyMethod o) {
        return (identifier+name).compareTo(o.identifier+o.name);
    }
}
