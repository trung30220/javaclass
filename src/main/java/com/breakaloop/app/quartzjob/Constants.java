package com.breakaloop.app.quartzjob;

public class Constants {
    public static final String TABLE_PREFIX = "job_";
    public static final String CRON = "cron";
    public static final String FIRE_TIME = "fireTime";
    public static final String JOB_PACKAGE_ENTITY = "com.breakaloop.app.quartzjob";
    public static final String JOB_PACKAGE_REPOSITORY = "com.breakaloop.app.quartzjob";
    public static final String JOB_PACKAGE_SERVICE = "com.breakaloop.app.quartzjob";
}
