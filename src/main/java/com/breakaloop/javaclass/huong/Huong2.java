package com.breakaloop.javaclass.huong;

import java.io.File;
import java.io.IOException;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
public class Huong2 {
    public static void main(String args[]) throws IOException {
        BufferedImage img = null;
        File file;
        try {
            file = new File("/Users/trung/Downloads/59301726_824506491256499_7237234689369440256_n.jpg");
            img = ImageIO.read(file);
//xác định width và height
            int width = img.getWidth();
            int height = img.getHeight();
//
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
//lấy màu của vị trí đó và chuyển thành màu xám
// get màu
                    int color = img.getRGB(i, j);
// dịch bit và chuyển thành hệ 16
                    int alpha = color >> 24&0xff;
                    int blue = color&0xff ;
                    int red = color >> 16 &0xff;
                    int green = color >> 8&0xff;
// màu xám sẽ bằng trung bình 3 màu kìa
                    int gray = (blue + green + red) / 3;
// set màu bằng toàn bộ màu xám
                    color = (alpha<<24) | (gray<<16) | (gray<<8) | gray;
//set màu cho vi tri đó
                    img.setRGB(i,j,color);
                }
            }
        } catch (IOException e) {
            System.out.println("Có lối trong phần đọc file");
        }
        try {
            file = new File("BonBon_1.jpg");
            ImageIO.write(img, "jpg", file);
        } catch (IOException e) {
            System.out.println("Có lỗi trong phần tạo ra ảnh mơi");
        }
    }
}