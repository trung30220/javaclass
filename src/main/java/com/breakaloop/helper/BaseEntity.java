package com.breakaloop.helper;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
@MappedSuperclass
public class BaseEntity extends ValidObject implements Serializable, Comparable<BaseEntity> {
    @Column
    @Id
    protected String id;

    public BaseEntity(String id) {
        this.id = id;
    }

    public BaseEntity() {
        generateId();
    }

    @PostPersist
    @PostUpdate
    public void generateId() {
        id = id==null?UUID.randomUUID().toString():id;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof BaseEntity) {
            return id.equals(((BaseEntity) obj).id);
        } else return false;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public int compareTo(BaseEntity o) {
        return id.compareTo(o.id);
    }

//    public BaseEntity() {
//        id = UUIDUtils.generateId2(id);
//        uuid = genUuid();
//        bigint = genBigint();
//    }
//
//    @Column
//    @Id
//    byte[] id;
//
//    @Transient
//    String uuid;
//
//    @JsonFormat(shape=JsonFormat.Shape.STRING)
//    @Transient
//    BigInteger bigint;
//
//    public String getUuid() {
//        uuid = genUuid();
//        return uuid;
//    }
//
//    public void setUuid(String uuid) {
//        this.uuid = uuid;
//        UUID fromString = UUID.fromString(uuid);
//        this.id = UUIDUtils.asBytes(fromString);
//        this.bigint = UUIDUtils.convertToBigInteger(fromString);
//    }
//
//    public BigInteger getBigint() {
//        bigint = genBigint();
//        return bigint;
//    }
//
//    public void setBigint(BigInteger bigint) {
//        this.bigint = bigint;
//        UUID fromBigInteger = UUIDUtils.convertFromBigInteger(bigint);
//        this.id = UUIDUtils.asBytes(fromBigInteger);
//        this.uuid = fromBigInteger.toString();
//    }
//
//    String genUuid() {
//        return uuid!=null?uuid:UUIDUtils.asUuid(id).toString();
//    }
//    BigInteger genBigint() {
//        return bigint!=null?bigint:UUIDUtils.convertToBigInteger(UUIDUtils.asUuid(id));
//    }
}
