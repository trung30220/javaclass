import React, { Component } from 'react';
import { Segment, Form } from 'semantic-ui-react'
import { getQueryString, updateQueryString, axiosGet, copyToClipboard } from '../../utils'
import { MyContext } from '../lazy'
import TeamList from './team-list'
import TeamCreate from './team-create'
import TeamAddMember from './team-add-member'
import TeamFind from './team-find'

export default class TeamManager extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
        this.loadTeamList = this.loadTeamList.bind(this);
    }

    handleChangeUser(event) {
        this.setState({user: event.target.value});
    }
    handleChangeMessage(event) {
        this.setState({message: event.target.value});
    }

    loadTeamList(storage, updateStorage) {
        let self = this
        axiosGet('/api/san-ball-7/team/list', null,
            function(response) {
                storage.teamListResponse = response;
                storage.teamList = response.data.content;
                updateStorage(storage);
            }, function(response) {
                console.log(response);
            }
        );
    }

    render() {
        return (
            <MyContext.Consumer>
                {({storage, updateStorage}) => (
                    <Segment basic>
                        <Segment.Group>
                            <Segment><TeamCreate/></Segment>
                            <Segment><TeamList/></Segment>
                            <Segment><TeamAddMember/></Segment>
                            <Segment><TeamFind/></Segment>
                        </Segment.Group>
                    </Segment>
                )}
            </MyContext.Consumer>
        )
    }
}
