package com.breakaloop.repository;

import com.breakaloop.entity.MonMan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

//public interface MonManRepository extends CrudRepository<MonMan,String> {
public interface MonManRepository extends JpaRepository<MonMan, Long> {
    @Query("from MonMan where createdBy = :userId order by id desc")
    List<MonMan> findByUserId(@Param("userId") String userId);
}
