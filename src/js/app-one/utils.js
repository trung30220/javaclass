import React, { Component } from 'react'
import { Header, Segment, Container, Form, Tab, Table } from 'semantic-ui-react'
import { axiosGet, axiosPost } from '../utils'

export function matchesToTable(responseByDay) {
    let pitchByDay = responseByDay.content;
    console.log(pitchByDay);

    let matchMaxNum = 0;
    if (pitchByDay) {
        for (const item of pitchByDay.hours) {
            if (item.matches.length > matchMaxNum) {
                matchMaxNum = item.matches.length
            }
        }
    }

    let matchHeader = []
    for (let i = 0; i < matchMaxNum; i++) {
        matchHeader.push(<Table.HeaderCell key={i}>Pitch {i}</Table.HeaderCell>);
    }

    return (
        <Table collapsing>
            <Table.Header>
                <Table.Row>
                    <Table.HeaderCell>Start Date</Table.HeaderCell>
                    <Table.HeaderCell>End Date</Table.HeaderCell>
                    {matchHeader}
                </Table.Row>
            </Table.Header>

            <Table.Body>
                {pitchByDay && pitchByDay.hours.map(function (element, index) {
                    let matches = []
                    for (let i = 0; i < element.matches.length; i++) {
                        let match = element.matches[i];
                        if (match.teams.length == 0) {
                            matches.push(<Table.Cell key={i}>x</Table.Cell>);
                        } else if (match.teams.length == 1) {
                            matches.push(<Table.Cell key={i}>{match.teams[0].name}</Table.Cell>);
                        } else if (match.teams.length == 2) {
                            matches.push(<Table.Cell key={i}>{match.teams[0].name + ' vs. ' + match.teams[1].name}</Table.Cell>);
                        } else if (match.teams.length > 2) {
                            matches.push(<Table.Cell key={i}>TOO MUCH</Table.Cell>);
                        }
                    }
                    for (let i = 0; i < matchMaxNum-element.matches.length; i++) {
                        matches.push(<Table.Cell key={i}>...</Table.Cell>);
                    }
                    return (
                        <Table.Row key={index}>
                            <Table.Cell>{element.startTime}</Table.Cell>
                            <Table.Cell>{element.endTime}</Table.Cell>
                            {matches}
                        </Table.Row>
                    );
                })}
            </Table.Body>
        </Table>
    )
}

//            <Table.Footer>
//                <Table.Row>
//                    <Table.HeaderCell></Table.HeaderCell>
//                </Table.Row>
//            </Table.Footer>
