package com.breakaloop.app.quartzjob;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Trigger;

import java.util.*;

import static org.quartz.JobBuilder.newJob;

public class JobDescriptor<T extends Job> {

    private String name;

    private String group;

    private Map<String, Object> data = new LinkedHashMap<>();// store data to handle job

    private List<TriggerDescriptor> triggerDescriptors = new ArrayList<>();

    public JobDescriptor() {

    }

    public JobDescriptor(String name, String group, Map<String, Object> data, List<TriggerDescriptor> triggerDescriptors) {
        this.name = name;
        this.group = group;
        this.data = data;
        this.triggerDescriptors = triggerDescriptors;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }

    public List<TriggerDescriptor> getTriggerDescriptors() {
        return triggerDescriptors;
    }

    public void setTriggerDescriptors(List<TriggerDescriptor> triggerDescriptors) {
        this.triggerDescriptors = triggerDescriptors;
    }

    public Set<Trigger> buildTriggers() {
        Set<Trigger> triggers = new LinkedHashSet<>();
        for (TriggerDescriptor triggerDescriptor : triggerDescriptors) {
            triggers.add(triggerDescriptor.buildTrigger());
        }

        return triggers;
    }

    public JobDetail buildJobDetail(Class <T> t) {
        JobDataMap jobDataMap = new JobDataMap(this.data);
        return newJob(t).withIdentity(this.name, this.group).usingJobData(jobDataMap).build();
    }

    /**
     * Convenience method that builds a descriptor from JobDetail and Trigger(s)
     *
     * @param jobDetail     the JobDetail instance
     * @param triggersOfJob the Trigger(s) to associate with the Job
     * @return the JobDescriptor
     */
    public static JobDescriptor buildDescriptor(JobDetail jobDetail, List<? extends Trigger> triggersOfJob) {
        List<TriggerDescriptor> triggerDescriptors = new ArrayList<>();

        triggersOfJob.forEach(trigger -> triggerDescriptors.add(TriggerDescriptor.buildDescriptor(trigger)));

        JobDescriptor jobDescriptor = new JobDescriptor();
        jobDescriptor.setName(jobDetail.getKey().getName());
        jobDescriptor.setGroup(jobDetail.getKey().getGroup());
        jobDescriptor.setTriggerDescriptors(triggerDescriptors);

        return jobDescriptor;
    }
}
