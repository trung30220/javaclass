import React, { Component } from 'react';
import { Segment, Form, Dropdown } from 'semantic-ui-react'
import { getQueryString, updateQueryString, axiosGet, axiosPost, copyToClipboard } from '../../utils'
import { MyContext } from '../lazy'
import UtilsChosePitchChild from './utils-chose-pitch-child'

export default class ScheduleEditAll extends Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.loadAll = this.loadAll.bind(this);
    }

    handleChange(event, { name, value }, storage) {
        storage.scheduleEditAll.request = {...storage.scheduleEditAll.request, [name]: event.target.value};
        storage.updateStorage(storage);
    }

    loadAll(storage) {
        axiosGet('/api/san-ball-7/schedule/all?pitchChildId=' + storage.scheduleEditAll.pitchChildId, null,
            function(response) {
                storage.scheduleEditAll.response = response.data;
                storage.updateStorage(storage);
            }, function(response) {
                console.log(response);
            });
    }

    getOptions(storage) {
        let list = storage.pitchList.response.content;
        const options = _.map(list, (item, index) => ({
            key: item.id,
            text: item.name,
            value: item.id,
        }))
        return options;
    }

    render() {
        return (
            <MyContext.Consumer>
                {({storage}) => (
                    <div>
                        <Form>
                            <UtilsChosePitchChild localStorage={storage.scheduleEditAll}/>
                            <Form.Group>
                                <Form.Field>
                                <Form.Button content='Load All Schedules'
                                    onClick={() => this.loadAll(storage)} />
                                </Form.Field>
                            </Form.Group>
                        </Form>
                        <strong>request:</strong>
                        <Segment style={{ overflow: 'auto', maxHeight: '27em' }}>
                            <pre>{JSON.stringify(storage.scheduleEditAll.request, null, 2)} </pre>
                        </Segment>
                        <strong>response:</strong>
                        <Segment style={{ overflow: 'auto', maxHeight: '27em' }}>
                            <pre>{JSON.stringify(storage.scheduleEditAll.response.content, null, 2)} </pre>
                        </Segment>
                    </div>
                )}
            </MyContext.Consumer>
        )
    }
}
