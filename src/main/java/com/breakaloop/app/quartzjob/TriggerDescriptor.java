package com.breakaloop.app.quartzjob;

import org.quartz.JobDataMap;
import org.quartz.Trigger;

import java.util.Date;
import java.util.TimeZone;

import static com.breakaloop.app.quartzjob.Constants.CRON;
import static com.breakaloop.app.quartzjob.Constants.FIRE_TIME;
import static java.time.ZoneId.systemDefault;
import static java.util.UUID.randomUUID;
import static org.quartz.CronExpression.isValidExpression;
import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;
import static org.springframework.util.StringUtils.isEmpty;

public class TriggerDescriptor {

    private String name;

    private String group;

    private Date fireTime;

    private String cron;

    public TriggerDescriptor() {

    }

    public TriggerDescriptor(String name, String group, Date fireTime) {
        this.name = name;
        this.group = group;
        this.fireTime = fireTime;
    }

    public TriggerDescriptor setName(final String name) {
        this.name = name;
        return this;
    }

    public TriggerDescriptor setGroup(final String group) {
        this.group = group;
        return this;
    }

    public TriggerDescriptor setFireTime(final Date fireTime) {
        this.fireTime = fireTime;
        return this;
    }

    public TriggerDescriptor setCron(final String cron) {
        this.cron = cron;
        return this;
    }

    private String buildName() {
        return isEmpty(name) ? randomUUID().toString() : name;
    }

    public Trigger buildTrigger() {
        if (!isEmpty(cron)) {
            if (!isValidExpression(cron))
                throw new IllegalArgumentException("Provided expression " + cron + " is not a valid cron expression");
            return newTrigger().withIdentity(buildName(), group).withSchedule(cronSchedule(cron)
                    .withMisfireHandlingInstructionFireAndProceed().inTimeZone(TimeZone.getTimeZone(systemDefault())))
                    .usingJobData(CRON, cron).build();
        } else if (!isEmpty(fireTime)) {
            JobDataMap jobDataMap = new JobDataMap();
            jobDataMap.put(FIRE_TIME, fireTime);
            return newTrigger().withIdentity(buildName(), group)
                    .withSchedule(simpleSchedule().withMisfireHandlingInstructionNextWithExistingCount())
                    .startAt(fireTime).usingJobData(jobDataMap).build();
        }
        throw new IllegalStateException("unsupported trigger descriptor " + this);
    }

    public static TriggerDescriptor buildDescriptor(Trigger trigger) {
        return new TriggerDescriptor().setName(trigger.getKey().getName()).setGroup(trigger.getKey().getGroup())
                .setFireTime((Date) trigger.getJobDataMap().get(FIRE_TIME))
                .setCron(trigger.getJobDataMap().getString(CRON));
    }
}
