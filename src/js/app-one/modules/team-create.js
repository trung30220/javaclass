import React, { Component } from 'react';
import { Segment, Form } from 'semantic-ui-react'
import { getQueryString, updateQueryString, axiosGet, axiosPost, copyToClipboard } from '../../utils'
import { MyContext } from '../lazy'

export default class TeamCreateManager extends Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.create = this.create.bind(this);
    }

    handleChange(event, { name, value }, storage) {
        storage.teamCreate.request = {...storage.teamCreate.request, [name]: event.target.value};
        storage.updateStorage(storage);
    }

    create(storage) {
        axiosPost('/api/san-ball-7/team/create', storage.teamCreate.request,
            function(response) {
                    storage.teamCreate.response = response;
                    storage.updateStorage(storage);
                }, function(response) {
                    console.log(response);
                }
            );
    }

    render() {
        return (
            <MyContext.Consumer>
                {({storage}) => (
                    <div>
                        <Form>
                            <Form.Group>
                                <Form.Field width={8}>
                                <Form.Input label='name' name='name'
                                    value={storage.teamCreate.request.name}
                                    onChange={(event, data) =>
                                        this.handleChange(event, data, storage)} />
                                </Form.Field>
                                <Form.Field width={8}>
                                <Form.Input label='password'name='password'
                                    value={storage.teamCreate.request.password}
                                    onChange={(event, data) =>
                                        this.handleChange(event, data, storage)} />
                                </Form.Field>
                            </Form.Group>
                            <Form.Group>
                                <Form.Field>
                                <Form.Button content='Create Team'
                                    onClick={() => this.create(storage)} />
                                </Form.Field>
                            </Form.Group>
                        </Form>
                        <strong>request:</strong>
                        <Segment style={{ overflow: 'auto', maxHeight: '27em' }}>
                            <pre>{JSON.stringify(storage.teamCreate.request, null, 2)} </pre>
                        </Segment>
                        <strong>response:</strong>
                        <Segment style={{ overflow: 'auto', maxHeight: '27em' }}>
                            <pre>{JSON.stringify(storage.teamCreate.response.data, null, 2)} </pre>
                        </Segment>
                    </div>
                )}
            </MyContext.Consumer>
        )
    }
}
