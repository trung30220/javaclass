package com.breakaloop.helper;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Getter
@Setter
public class ResponseWrapper<T> {
    Integer code;
    String message;
    T content;

    Long totalElements;
    Integer totalPages;
    Integer numberOfElements; // current size of content
    Integer page;
    Integer size;

    public ResponseWrapper(HttpStatus code, String message, T content) {
        this.code = code.value();
        this.message = message;
        this.content = content;
    }
    public ResponseWrapper(HttpStatus code, String message, Integer page, Integer size,
                           Long totalElements, Integer totalPages, Integer numberOfElements,
                           T content) {
        this.code = code.value();
        this.message = message;
        this.content = content;
        this.page = page;
        this.size = size;
        this.totalElements = totalElements;
        this.totalPages = totalPages;
        this.numberOfElements = numberOfElements;
    }
}
