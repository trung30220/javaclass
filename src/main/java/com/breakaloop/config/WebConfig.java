package com.breakaloop.config;

import com.breakaloop.interceptor.LogInterceptor;
import com.breakaloop.filter.AuthFilter;
import com.breakaloop.filter.LimitAccessFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.web.servlet.config.annotation.*;

@Configuration
@EnableWebMvc
public class WebConfig implements WebMvcConfigurer {//WebMvcConfigurerAdapter {

    @Autowired
    LogInterceptor logInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(logInterceptor);
    }

    @Value("${static.resources.filepath}")
    private String staticFilepath;

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Override
    public void configureDefaultServletHandling(final DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }

    @Override
    public void addViewControllers(final ViewControllerRegistry registry) {
        //super.addViewControllers(registry);
        registry.addViewController("/").setViewName("forward:/index");
        registry.addViewController("/index");
        registry.addViewController("/login");

        // forward all web to ui
        registry.addViewController("/web/**").setViewName("forward:/index");
    }

    @Override
    public void addResourceHandlers(final ResourceHandlerRegistry registry) {
        //registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");

        // in main/resources
        registry.addResourceHandler("/resources/**").addResourceLocations("classpath:/static/");

        registry.addResourceHandler("/**").addResourceLocations("classpath:/cert/");

//        // staticFilepath can be any path
//        registry.addResourceHandler("/resources/**").addResourceLocations(staticFilepath);

        // swagger
        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");

        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
    }

    @Bean
    public FilterRegistrationBean apiFilter() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(new AuthFilter());
        registration.setOrder(1);
        registration.addUrlPatterns("/**");// "/api/*"); // ("/api/**","/ping","/api/*");
        return registration;
    }

    @Bean
    public FilterRegistrationBean limitAccessFilter() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(new LimitAccessFilter());
        registration.setOrder(0);
        registration.addUrlPatterns("/**");
        return registration;
    }
}