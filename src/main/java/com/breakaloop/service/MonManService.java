package com.breakaloop.service;

import com.breakaloop.entity.MonMan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface MonManService {
    MonMan save(MonMan mm);

    List<MonMan> findByUserId(String userId);

    MonMan findById(Long id);

    void delete(MonMan monMan);

    Page<MonMan> findPaginated(Pageable pageable);
}
