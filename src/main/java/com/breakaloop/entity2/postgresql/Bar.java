package com.breakaloop.entity2.postgresql;


import javax.persistence.*;

@Entity
@Table(name = "BAR")
public class Bar {

    @Id
    @GeneratedValue
    @Column(name = "BaseEntity")
    private Long id;

    @Column(name = "BAR")
    private String bar;

    Bar(String bar) {
        this.bar = bar;
    }

    Bar() {
        // Default constructor needed by JPA
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBar() {
        return bar;
    }

    public void setBar(String bar) {
        this.bar = bar;
    }
}