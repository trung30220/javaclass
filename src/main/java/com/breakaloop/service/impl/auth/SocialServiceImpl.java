package com.breakaloop.service.impl.auth;

import com.breakaloop.helper.JsonHelper;
import com.breakaloop.service.SocialService;
import com.breakaloop.service.UserProcessor;
import com.nimbusds.jose.JOSEException;
import org.glassfish.jersey.internal.guava.Preconditions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.text.ParseException;
import java.util.Map;

import static com.breakaloop.constant.Constants.*;

@Service
public class SocialServiceImpl implements SocialService {

    @Autowired
    private UserProcessor userProcessor;

    @Value("${spring.social.facebook.appId}")
    private String facebookAppId;
    @Value("${spring.social.facebook.appSecret}")
    private String facebookAppSecret;

    private JsonHelper jsonHelper = new JsonHelper();

    private Client restClient;

    public SocialServiceImpl() {
        restClient = ClientBuilder.newClient();
    }

    @Override
    public ResponseEntity loginV3(final String redirectUri, final String code, final String state,
                                  final String authHeader, final String remoteHost) throws ParseException, JOSEException, IOException {
//        final String accessTokenUrl = "https://www.facebook.com/v3.0/dialog/oauth";
//        final String graphApiUrl = "https://graph.facebook.com/v2.3/me";
//        final String userPictureUrl = "https://graph.facebook.com/v2.3/{user-id}/picture";
//        Response response = restClient.target(accessTokenUrl).queryParam(CLIENT_ID_KEY, facebookAppId)
//                .queryParam(REDIRECT_URI_KEY, redirectUri)
//                .queryParam(STATE, state)
//                //.queryParam(CLIENT_SECRET, facebookAppSecret)
//                //.queryParam(CODE_KEY, code)
//                .request(MediaType.TEXT_PLAIN)
//                .accept(MediaType.TEXT_PLAIN).get();
//
//        final String paramStr = Preconditions.checkNotNull(response.readEntity(String.class));
//        final Map<String, Object> apiInfo = jsonHelper.getJsonMap(paramStr);

        return null;
    }

    @Override
    public ResponseEntity login(final String redirectUri, final String code,
                                final String authHeader, final String remoteHost) throws ParseException, JOSEException, IOException {
        final String accessTokenUrl = "https://graph.facebook.com/v2.3/oauth/access_token";
        final String graphApiUrl = "https://graph.facebook.com/v2.3/me";
        final String userPictureUrl = "https://graph.facebook.com/v2.3/{user-id}/picture";
        Response response;

        // Step 1. Exchange authorization code for access token.
        response
                = restClient.target(accessTokenUrl).queryParam(CLIENT_ID_KEY, facebookAppId)
                .queryParam(REDIRECT_URI_KEY, redirectUri)
                .queryParam(CLIENT_SECRET, facebookAppSecret)
                .queryParam(CODE_KEY, code).request("text/plain")
                .accept(MediaType.TEXT_PLAIN).get();

        final String paramStr = Preconditions.checkNotNull(response.readEntity(String.class));
        final Map<String, Object> apiInfo = jsonHelper.getJsonMap(paramStr);

        response
                = restClient.target(graphApiUrl).queryParam("access_token", apiInfo.get("access_token").toString())
                .queryParam("expires_in", apiInfo.get("expires_in").toString()).request("text/plain").get();

        final Map<String, Object> userInfo = jsonHelper.getResponseEntity(response);

        String userId = userInfo.get("id").toString();
        String userIdPictureUrl = userPictureUrl.replace("{user-id}", userId);

        // Step 3. Process the authenticated the user.
        return userProcessor.processUser(authHeader, remoteHost, "facebook",
                userInfo.get("id").toString(),
                userInfo.get("name").toString(),
                userInfo.get("account").toString(),
                userIdPictureUrl,
                userInfo.get("name").toString(),
                userInfo.get("first_name").toString(),
                userInfo.get("last_name").toString());
    }
}
