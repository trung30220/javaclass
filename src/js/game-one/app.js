import React, { Component } from 'react';
import SockJS from "sockjs-client";
//import Stomp from "@stomp/stompjs";
import Stomp from "stompjs";

var colors = [
    '#2196F3', '#32c787', '#00BCD4', '#ff5652',
    '#ffc107', '#ff85af', '#FF9800', '#39bbb0'
];

class GameOneApp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            chatPage: null,
            messageForm: null,
            messageInput: null,
            messageArea: null,
            connectingElement: null,

            stompClient: null,
            username: null,
            user: '',
            message: '',
        }
        this.handleChangeUser = this.handleChangeUser.bind(this);
        this.handleChangeMessage = this.handleChangeMessage.bind(this);
        this.connect = this.connect.bind(this);
        this.onConnected = this.onConnected.bind(this);
        this.onError = this.onError.bind(this);
        this.sendMessage = this.sendMessage.bind(this);
        this.onMessageReceived = this.onMessageReceived.bind(this);
        this.getAvatarColor = this.getAvatarColor.bind(this);
        this.loadDiary = this.loadDiary.bind(this);
    }

    componentWillUnmount() {
        // todo revert add event
    }

    loadDiary() {
    }

    componentDidMount() {
//        loadObjectUrl('https://cdnjs.cloudflare.com/ajax/libs/sockjs-client/1.1.4/sockjs.min.js')
//            .then(function() {
//                console.log('"sockjs" loaded!');
//            })
//            .catch(function(err) {
//                console.error('Something went wrong!', err);
//            })

        this.state.chatPage = document.querySelector('#chat-page');
        this.state.messageForm = document.querySelector('#messageForm');
        this.state.messageInput = document.querySelector('#message');
        this.state.messageArea = document.querySelector('#messageArea');
        this.state.connectingElement = document.querySelector('.connecting');

//        this.state.usernameForm.addEventListener('submit', this.connect, true);
//        this.state.messageForm.addEventListener('submit', this.sendMessage, true);
    }

    handleChangeUser(event) {
        this.setState({user: event.target.value});
    }
    handleChangeMessage(event) {
        this.setState({message: event.target.value});
    }

    connect(event) {
        this.state.username = this.state.user;//document.querySelector('#name').value.trim();

        if(this.state.username) {
//            this.state.usernamePage.classList.add('hidden');
            this.state.chatPage.classList.remove('hidden');

            var socket = new SockJS('/ws');
            this.state.stompClient = Stomp.over(socket);

            this.state.stompClient.connect({}, this.onConnected, this.onError);
        }
        event.preventDefault();
    }

    onConnected() {
        // Subscribe to the Public Topic
        this.state.stompClient.subscribe('/topic/public', this.onMessageReceived);

        // Tell your username to the server
        this.state.stompClient.send("/app/chat.addUser",
            {},
            JSON.stringify({sender: this.state.username, type: 'JOIN'})
        )

        //this.state.connectingElement.classList.add('hidden');
    }

    onError(error) {
        this.state.connectingElement.textContent = 'Could not connect to WebSocket server. Please refresh this page to try again!';
        this.state.connectingElement.style.color = 'red';
    }

    sendMessage(event) {
        var messageContent = this.state.messageInput.value.trim();
        if(messageContent && this.state.stompClient) {
            var chatMessage = {
                sender: this.state.username,
                content: this.state.messageInput.value,
                type: 'CHAT'
            };
            this.state.stompClient.send("/app/chat.sendMessage", {}, JSON.stringify(chatMessage));
            this.state.messageInput.value = '';
        }
        event.preventDefault();
    }

    onMessageReceived(payload) {
        var message = JSON.parse(payload.body);

        var messageElement = document.createElement('li');

        if(message.type === 'JOIN') {
            messageElement.classList.add('event-message');
            message.content = message.sender + ' joined!';
        } else if (message.type === 'LEAVE') {
            messageElement.classList.add('event-message');
            message.content = message.sender + ' left!';
        } else {
            messageElement.classList.add('chat-message');

            var avatarElement = document.createElement('i');
            var avatarText = document.createTextNode(message.sender[0]);
            avatarElement.appendChild(avatarText);
            avatarElement.style['background-color'] = this.getAvatarColor(message.sender);

            messageElement.appendChild(avatarElement);

            var usernameElement = document.createElement('span');
            var usernameText = document.createTextNode(message.sender);
            usernameElement.appendChild(usernameText);
            messageElement.appendChild(usernameElement);
        }

        var textElement = document.createElement('p');
        var messageText = document.createTextNode(message.content);
        textElement.appendChild(messageText);

        messageElement.appendChild(textElement);

        messageArea.appendChild(messageElement);
        messageArea.scrollTop = messageArea.scrollHeight;
    }

    getAvatarColor(messageSender) {
        var hash = 0;
        for (var i = 0; i < messageSender.length; i++) {
            hash = 31 * hash + messageSender.charCodeAt(i);
        }
        var index = Math.abs(hash % colors.length);
        return colors[index];
    }

    render() {
        let gridStyle = mc12;
        return (
            <div>
                <div style={gridStyle}>
                    <div style={mc} onClick={() => this.test('chat')}>Chat{this.props.smallScreen}aaaa</div>
                </div>
                <div style={gridStyle}>
                    <div style={mc} onClick={() => this.props.closeApp()}>Exit</div>
                </div>

                <div style={gridStyle}>
                    <div style={mc}>
                        <input type="text" id="name" placeholder="Username" autoComplete="off" className="form-control"
                         value={this.state.user} onChange={this.handleChangeUser} style={{width:'100%',boxSizing:"border-box"}} />
                    </div>
                </div>
                <div style={gridStyle}>
                    <div style={mc} onClick={(e) => this.connect(e)}>Start Chatting</div>
                </div>

                <div style={gridStyle}>
                    <div style={mc}>
                        <input type="text" id="message" placeholder="Type a message..." autoComplete="off" className="form-control"
                         value={this.state.message} onChange={this.handleChangeMessage} style={{width:'100%',boxSizing:"border-box"}} />
                    </div>
                </div>
                <div style={gridStyle}>
                    <div style={mc} onClick={(e) => this.sendMessage(e)}>Send</div>
                </div>

                <div id="chat-page" className="hidden">
                    <div className="chat-container">
                        <ul id="messageArea">
                        </ul>
                    </div>
                </div>

            </div>
        )
    }
}

const mc = {
    borderRadius: '10px',
    margin: '10px',
    padding: '10px',
    color: '#ffffff',
    fontSize: '20px',
    backgroundColor: '#808080',
    boxShadow: '0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24)',
}
const mc1  = {float: 'left', width: '8.33%'}
const mc2  = {float: 'left', width: '16.66%'}
const mc3  = {float: 'left', width: '25%'}
const mc4  = {float: 'left', width: '33.33%'}
const mc5  = {float: 'left', width: '41.66%'}
const mc6  = {float: 'left', width: '50%'}
const mc7  = {float: 'left', width: '58.33%'}
const mc8  = {float: 'left', width: '66.66%'}
const mc9  = {float: 'left', width: '75%'}
const mc10 = {float: 'left', width: '83.33%'}
const mc11 = {float: 'left', width: '91.66%'}
const mc12 = {float: 'left', width: '100%'}

//export default GameOne;
export default () => {
  return (<GameOneApp/>);
};
