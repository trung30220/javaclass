package com.breakaloop.repository2.mysql;

import com.breakaloop.entity2.mysql.Foo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FooRepository extends JpaRepository<Foo, Long> {

    Optional<Foo> findById(Long id);

}
