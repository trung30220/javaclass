/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.breakaloop.controller;

import com.breakaloop.dto.Payload;
import com.breakaloop.helper.AuthUtils;
import com.breakaloop.service.SocialService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.nimbusds.jose.JOSEException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import java.io.IOException;
import java.text.ParseException;

//import jersey.repackaged.com.google.common.base.Preconditions;
//import org.apache.log4j.Logger;

/**
 *
 * @author Carlos
 */
@RestController
public class FacebookController {    
//    private static Logger LOG = Logger.getLogger(FacebookController.class);

    @Autowired
    SocialService socialService;

    @GetMapping("/auth/facebook")
    public ResponseEntity loginFacebook(@RequestBody final Payload payload,
                                        @Context final HttpServletRequest request) throws JsonParseException, JsonMappingException,
            IOException, ParseException, JOSEException {
        return socialService.login(payload.getRedirectUri(), payload.getCode(),
                request.getHeader(AuthUtils.AUTH_HEADER_KEY), request.getRemoteHost());
    }

}
