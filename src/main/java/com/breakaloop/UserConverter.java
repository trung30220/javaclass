package com.breakaloop;

import com.breakaloop.dto.UserDto;
import com.breakaloop.entity.MyUser;

import java.util.stream.Collectors;

public class UserConverter {
    public static MyUser dtoToEntity(UserDto userDto) {
        MyUser myUser = new MyUser(userDto.getUserName(), null);
        myUser.setUserId(userDto.getUserId());
        myUser.setSkills(userDto.getSkillDtos().stream().map(SkillConverter::dtoToEntity).collect(Collectors.toList()));
        return myUser;
    }
    public static UserDto entityToDto(MyUser myUser) {
        UserDto userDto = new UserDto(myUser.getUserId(), myUser.getUserName(), null);
        userDto.setSkillDtos(myUser.getSkills().stream().map(SkillConverter::entityToDto).collect(Collectors.toList()));
        return userDto;
    }
}

