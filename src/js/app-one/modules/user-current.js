import React, { Component } from 'react';
import { Segment, Form, Checkbox } from 'semantic-ui-react'
import { getQueryString, updateQueryString, axiosGet, axiosPost, copyToClipboard } from '../../utils'
import { MyContext } from '../lazy'

export default class CurrentUserManager extends Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.showMessage = this.showMessage.bind(this);
        this.update = this.update.bind(this);
        this.createUser = this.createUser.bind(this);
        this.login = this.login.bind(this);
        this.checkExist = this.checkExist.bind(this);
        this.state = {
            owner: false,
        }
        this.ownerSwitch = this.ownerSwitch.bind(this);
    }

    handleChange(event, { name, value }, storage) {
        storage.currentUser.request = {...storage.currentUser.request, [name]: event.target.value};
        storage.updateStorage(storage);
    }

    createUser(storage) {
        axiosPost('/api/san-ball-7/user/register', storage.currentUser.request,
            (response) => this.update(response, storage), this.showMessage);
    }

    login(storage) {
        axiosPost('/api/san-ball-7/user/login', storage.currentUser.request,
            (response) => this.update(response, storage), this.showMessage);
    }

    checkExist(storage) {
        axiosGet('/api/san-ball-7/user/exist?account='
                    + storage.currentUser.request.account, null,
            function(response){
                console.log(response)
                alert(JSON.stringify(response.data.content))
            }, function(response) {
                alert(JSON.stringify(response))
            });
    }

    update(response, storage) {
        storage.currentUser.response = response.data;
        storage.updateStorage(storage);
    }

    showMessage() {
    }

    ownerSwitch(event, data, storage) {
        this.state.owner = !this.state.owner;
        if (!this.state.owner) {
            storage.currentUser.request.pitchChildNum = 0;
            storage.currentUser.request.pitchName = '';
            storage.currentUser.request.latitude = 0;
            storage.currentUser.request.longitude = 0;
        }
        storage.updateStorage(storage);
    }

    render() {
        // console.log(JSON.parse(JSON.stringify(questionGlobal)));
        return (
            <MyContext.Consumer>
                {({storage}) => (
                    <div>
                        <Form>
                            <Form.Group>
                                <Form.Field width={8}>
                                <Form.Input label='account' name='account'
                                    value={storage.currentUser.request.account}
                                    onChange={(event, data) =>
                                        this.handleChange(event, data, storage)} />
                                </Form.Field>
                                <Form.Field width={8}>
                                <Form.Input label='password' name='password'
                                    value={storage.currentUser.request.password}
                                    onChange={(event, data) =>
                                        this.handleChange(event, data, storage)} />
                                </Form.Field>
                            </Form.Group>
                            <Form.Group>
                                <Form.Field width={8}>
                                <Form.Input label='avatar' name='avatar'
                                    value={storage.currentUser.request.avatar?
                                                storage.currentUser.request.avatar.id:''}
                                    onChange={(event, data) => {
                                            storage.currentUser.request.avatar =
                                                JSON.parse('{"id":"'+event.target.value+'"}');
                                            storage.updateStorage(storage);
                                        }} />
                                </Form.Field>
                            </Form.Group>
                            <Form.Field>
                                <Checkbox label='Create Pitch Owner?'
                                    onChange={(event, data) => this.ownerSwitch(event, data, storage)}/>
                            </Form.Field>
                            {this.state.owner &&
                                <div>
                                    <Form.Group>
                                        <Form.Field width={8}>
                                        <Form.Input label='latitude' name='latitude'
                                            value={storage.currentUser.request.latitude}
                                            onChange={(event, data) =>
                                                this.handleChange(event, data, storage)} />
                                        </Form.Field>
                                        <Form.Field width={8}>
                                        <Form.Input label='longitude' name='longitude'
                                            value={storage.currentUser.request.longitude}
                                            onChange={(event, data) =>
                                                this.handleChange(event, data, storage)} />
                                        </Form.Field>
                                    </Form.Group>
                                    <Form.Group>
                                        <Form.Field width={8}>
                                        <Form.Input label='Pitch Child Num' name='pitchChildNum'
                                            value={storage.currentUser.request.pitchChildNum}
                                            onChange={(event, data) =>
                                                this.handleChange(event, data, storage)} />
                                        </Form.Field>
                                        <Form.Field width={8}>
                                        <Form.Input label='Pitch Name' name='pitchName'
                                            value={storage.currentUser.request.pitchName}
                                            onChange={(event, data) =>
                                                this.handleChange(event, data, storage)} />
                                        </Form.Field>
                                    </Form.Group>
                                </div>
                            }
                            <Form.Group>
                                <Form.Field>
                                <Form.Button content='Register User'
                                    onClick={() => this.createUser(storage)} />
                                </Form.Field>
                                <Form.Field>
                                <Form.Button content='Login User'
                                    onClick={() => this.login(storage)} />
                                </Form.Field>
                                <Form.Field>
                                <Form.Button content='Check Exist'
                                    onClick={() => this.checkExist(storage)} />
                                </Form.Field>
                            </Form.Group>
                        </Form>
                        <strong>request:</strong>
                        <Segment style={{ overflow: 'auto', maxHeight: '27em' }}>
                            <pre> {JSON.stringify(storage.currentUser.request, null, 2)} </pre>
                        </Segment>
                        <strong>response:</strong>
                        <Segment style={{ overflow: 'auto', maxHeight: '27em' }}>
                            <pre> {JSON.stringify(storage.currentUser.response, null, 2)} </pre>
                        </Segment>
                    </div>
                )}
            </MyContext.Consumer>
        )
    }
}
