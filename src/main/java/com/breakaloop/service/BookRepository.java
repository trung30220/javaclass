package com.breakaloop.service;

import com.breakaloop.dto.Book;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

//@Repository
@Service
public class BookRepository {

    @Inject
    private RedisTemplate<String, Book> redisTemplate;

    public void save(Book book) {
        redisTemplate.opsForValue().set(book.getId(), book);
    }

    public Book findById(String key) {
        return redisTemplate.opsForValue().get(key);
    }

    public List<Book> findAll() {
        List<Book> books = new ArrayList<>();

        Set<String> keys = redisTemplate.keys("*");
        Iterator<String> it = keys.iterator();

        while(it.hasNext()){
            books.add(findById(it.next()));
        }

        return books;
    }

    public void delete(Book b) {
        String key = b.getId();
        redisTemplate.opsForValue().getOperations().delete(key);
    }


    public void deleteAll() {
        Set<String> keys = redisTemplate.keys("*");
        Iterator<String> it = keys.iterator();

        while(it.hasNext()){
            Book b = new Book(it.next());
            delete(b);
        }
    }
}