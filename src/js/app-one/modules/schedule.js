import React, { Component } from 'react';
import { Segment, Form } from 'semantic-ui-react'
import { getQueryString, updateQueryString, axiosGet, copyToClipboard } from '../../utils'
import { MyContext } from '../lazy'
import ScheduleList from './schedule-list'
import ScheduleCreate from './schedule-create'
import ScheduleEditAll from './schedule-edit-all'

export default class Schedule extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        return (
            <MyContext.Consumer>
                {({storage, updateStorage}) => (
                    <Segment basic>
                        <Segment.Group>
                            <Segment><ScheduleList/></Segment>
                            <Segment><ScheduleCreate/></Segment>
                        </Segment.Group>
                        <Segment.Group>
                            <Segment><ScheduleEditAll/></Segment>
                        </Segment.Group>
                    </Segment>
                )}
            </MyContext.Consumer>
        )
    }
}
