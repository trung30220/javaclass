package com.breakaloop.constant;

public enum Gender {
    MALE, FEMALE, OTHER
}
