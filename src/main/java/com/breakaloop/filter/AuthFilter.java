package com.breakaloop.filter;

import com.breakaloop.helper.AuthUtils;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jwt.JWTClaimsSet;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.util.Map;

import static com.breakaloop.app.sanball7.constant.Constant.SANBALL7_APP;
import static com.breakaloop.constant.Constants.JWT_TYPE;

public class AuthFilter implements Filter {

    public static final String USER_ID = "userId";
    private static final String AUTH_ERROR_MSG = "Please make sure your request has an Authorization header",
            EXPIRE_ERROR_MSG = "Token has expired",
            JWT_ERROR_MSG = "Unable to parse JWT",
            JWT_INVALID_MSG = "Invalid JWT token",
            JWT_COOKIE_NAME = "MY_COOKIE";

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {

        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        String authHeader = httpRequest.getHeader(AuthUtils.AUTH_HEADER_KEY);

        if (StringUtils.isBlank(authHeader) || authHeader.split(" ").length != 2) {
            String uri = httpRequest.getRequestURI();
            if ("/".equals(uri)) {
                String queryString = httpRequest.getQueryString();
                if (queryString != null
                        && queryString.startsWith("token=")
                        && queryString.endsWith("&web=breakaloop.com")) {
                    String token = queryString.substring(6, queryString.length() - 19);
                    authHeader = "Bearer: " + token; // move token to header
                    processToken(request, response, httpResponse, chain, authHeader);
                } else {
                    authHeader = readTokenFromCookie(httpRequest);
                    if (authHeader == null) {
                        httpResponse.sendRedirect("/login");
                    } else processToken(request, response, httpResponse, chain, authHeader);
                }
            } else if ("/login".equals(uri)
                    || "/logout".equals(uri)
                    || "/signin/facebook".equals(uri)
                    || "/signin/google".equals(uri)
                    || "/error".equals(uri)
                    || "/swagger-ui.html".equals(uri) // swagger
                    || "/v2/api-docs".equals(uri) // swagger
//                    || "/swagger-resources".equals(uri) // swagger
//                    || uri.startsWith("/swagger-resources/") // swagger
                    || uri.startsWith("/swagger-resources") // swagger
                    || uri.startsWith("/webjars/") // swagger
                    || uri.startsWith("/.well-known/") // ssl
                    || uri.startsWith("/web/") // web
                    || uri.startsWith("/resources/")
                    || uri.startsWith("/ws/") // ws



                    // fixme
                    || uri.startsWith("/api/san-ball-7/")
                    || uri.startsWith("/job/")
                    || uri.startsWith("/api/health-check")



                    || "/topic/public".equals(uri) // ws
                    || "/app/chat.sendMessage".equals(uri) // ws
                    || "/app/chat.addUser".equals(uri)) { // ws
                chain.doFilter(request, response);
            } else {
                authHeader = readTokenFromCookie(httpRequest);
                if (authHeader == null) {
                    httpResponse.sendRedirect("/login");
                } else processToken(request, response, httpResponse, chain, authHeader);
            }
        } else processToken(request, response, httpResponse, chain, authHeader);
    }

    String readTokenFromCookie(HttpServletRequest httpRequest) {
        Cookie[] cookies = httpRequest.getCookies();
        if (cookies != null) {
            for (Cookie ck : cookies) {
                if (JWT_COOKIE_NAME.equals(ck.getName())) {
                    return "Bearer: " + ck.getValue();
                }
            }
        }
        return null;
    }

    private void processToken(ServletRequest request, ServletResponse response,
                              HttpServletResponse httpResponse, FilterChain chain, String authHeader)
            throws IOException, ServletException {
        JWTClaimsSet claimSet;
        try {
            claimSet = (JWTClaimsSet) AuthUtils.decodeToken(authHeader);
            Map<String, Object> customClaims = claimSet.getCustomClaims();
            Object jwtType = customClaims.get(JWT_TYPE);
            if (jwtType != null) {
                String type = (String) jwtType;
                if (SANBALL7_APP.equals(type)) {
                    request.setAttribute("data", customClaims);
                }
            }
            request.setAttribute(USER_ID, claimSet.getSubject());
        } catch (ParseException e) {
            //httpResponse.sendError(HttpServletResponse.SC_BAD_REQUEST, JWT_ERROR_MSG);
            System.out.println("AuthFilter: " + e.toString());
            httpResponse.sendRedirect("/login");
            return;
        } catch (JOSEException e) {
            //httpResponse.sendError(HttpServletResponse.SC_BAD_REQUEST, JWT_INVALID_MSG);
            System.out.println("AuthFilter: " + e.toString());
            httpResponse.sendRedirect("/login");
            return;
        }

        // ensure that the token is not expired
        if (new DateTime(claimSet.getExpirationTime()).isBefore(DateTime.now())) {
            //httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, EXPIRE_ERROR_MSG);
            System.out.println("AuthFilter: token expired");
            httpResponse.sendRedirect("/login");
        } else {
            chain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() { /* unused */ }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException { /* unused */ }
}
