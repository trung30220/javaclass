package com.breakaloop.javaclass.huong;

import javafx.application.Application;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
public class JavaFx extends Application {
    public static void main(String[] args) {
        launch(args);
    }
    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Color");
        String url = "C:\\Users\\huongbtt3\\Desktop\\anh\\Anh.jpg";
        FileInputStream inputstream = null;
        try {
            inputstream = new FileInputStream(url);
        } catch (FileNotFoundException e) {
// TODO Auto-generated catch block
            e.printStackTrace();
        }
        Image image = new Image(inputstream);
        ImageView imageView = new ImageView(image);
        imageView.setX(10);
        imageView.setY(10);
        imageView.setFitHeight(300);
        imageView.setFitWidth(300);
        Button buttonRotate = new Button("Black/White");
        buttonRotate.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                BufferedImage bi = setColor(url, true);
                imageView.setImage(SwingFXUtils.toFXImage(bi, null ));
            }
        });
        Button buttonW = new Button("Normal");
        buttonW.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                BufferedImage bi = setColor(url, null);
                imageView.setImage(SwingFXUtils.toFXImage(bi, null ));
            }
        });
        Button buttonScale = new Button("Gray");
        buttonScale.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                BufferedImage bi = setColor(url, false);
                imageView.setImage(SwingFXUtils.toFXImage(bi, null ));
            }
        });
        FlowPane root = new FlowPane();
        root.setPadding(new Insets(10));
        root.setHgap(10);
        root.getChildren().addAll(imageView, buttonRotate, buttonScale, buttonW);
        Scene scene = new Scene(root, 1000, 1000);
        primaryStage.setTitle("JavaFX ImageView");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    public BufferedImage setColor(String url,Boolean isBlackWhite) {
        BufferedImage img = null;
        File file = null;
        try {
            file = new File(url);
            img = ImageIO.read(file);
// xác định width và height
            int width = img.getWidth();
            int height = img.getHeight();
//
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
// get màu
                    int color = img.getRGB(i, j);
// dịch bit và chuyển thành hệ 16
                    int alpha = color >> 24&0xff;
                    int blue = color&0xff ;
                    int red = color >> 16 &0xff;
                    int green = color >> 8&0xff;
// màu xám sẽ bằng trung bình 3 màu kìa
                    if (isBlackWhite != null) {
                        int gray = (blue + green + red) / 3;
                        if (isBlackWhite) {
                            if (gray > 128) {
                                gray = 255;
                            } else {
                                gray = 0;
                            }
                        }
                        color = (alpha<<24) | (gray<<16) | (gray<<8) | gray;
                    }
// set màu bằng toàn bộ màu xám
//set màu cho vi tri đó
                    img.setRGB(i,j,color);
                }
            }
        } catch (IOException e) {
            System.out.println("Có lối trong phần đọc file");
        }
        try {
            file = new File("C:\\Users\\huongbtt3\\Desktop\\Anh_1.jpg");
            ImageIO.write(img, "jpg", file);
        } catch (IOException e) {
            System.out.println("Có lỗi trong phần tạo ra ảnh mơi");
        }
        return img;
    }
}
