package com.breakaloop.service.impl;

import com.breakaloop.entity.UserConnection;
import com.breakaloop.helper.AuthUtils;
import com.breakaloop.helper.Token;
import com.breakaloop.repository.UserConnectionRepository;
import com.breakaloop.service.UserProcessor;
import com.nimbusds.jose.JOSEException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static com.breakaloop.constant.Constants.NOT_FOUND_MSG;

@Component
public class UserProcessorImpl implements UserProcessor {

    @Autowired
    UserConnectionRepository userConnectionRepository;

    public ResponseEntity processUser(final String authHeader, final String remoteHost,
                                      final String providerId,
                                      final String providerUserId,
                                      final String userId,
                                      final String email, final String picture,
                                      final String name, final String givenName, final String familyName)
            throws JOSEException, ParseException {

        UserConnection userConnection = userConnectionRepository.findByProviderIdAndUserProviderId(providerId, providerUserId);

        //final String authHeader = request.getHeader(AuthUtils.AUTH_HEADER_KEY);
        if (StringUtils.isNotBlank(authHeader)) {
            if (userConnection == null) {
                return new ResponseEntity<>("not found",
                        HttpStatus.CONFLICT);
            }
            final String subject = AuthUtils.getSubject(authHeader); // <-- id UUID
            final UserConnection foundUserConnection = userConnectionRepository.findById(subject);
            if (foundUserConnection == null) {
                return new ResponseEntity<>(NOT_FOUND_MSG, HttpStatus.NOT_FOUND);
            }
        }

        String id = UUID.randomUUID().toString();
        Token token = AuthUtils.createToken(remoteHost, id);

        Map<String, Token> map = new HashMap<>();
        map.put(id, token);

        return new ResponseEntity<>(map, HttpStatus.OK);
    }
}
