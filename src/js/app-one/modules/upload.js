import React, { Component } from 'react';
import { Segment, Form } from 'semantic-ui-react'
import { getQueryString, updateQueryString, axiosPost, copyToClipboard } from '../../utils'
import { MyContext } from '../lazy'

export default class Upload extends Component {
    constructor(props) {
        super(props);
        this.state = {
            uploading: false,
            images: [],
        }
        this.onOneChange = this.onOneChange.bind(this);
        this.onMultipleChange = this.onMultipleChange.bind(this);
    }

    onMultipleChange(e, storage) {
        const files = Array.from(e.target.files)
        this.setState({ uploading: true })
        const formData = new FormData()
        files.forEach((file, i) => {
            formData.append(i, file)
            formData.append('file', file)
        })
        axiosPost('/api/san-ball-7/upload',
            formData,
            function(response) {
                console.log(response);
                storage.upload.response = response;
                storage.updateStorage(storage);
            }, function(response) {
                console.log(response);
            }
        );
    }

    onOneChange(e, storage) {
        this.setState({ uploading: true })
        const formData = new FormData()
        formData.append('file', e.target.files[0])
        axiosPost('/api/san-ball-7/upload',
            formData,
            function(response) {
                console.log(response);
                storage.upload.response = response;
                storage.updateStorage(storage);
            }, function(response) {
                console.log(response);
            }
        );
    }

    render() {
        return (
            <MyContext.Consumer>
                {({storage, updateStorage}) => (
                    <Segment basic>
                        <Segment.Group>
                            <Segment>
                                <input type='file' id='multi'
                                    onChange={(e) => this.onMultipleChange(e,storage)} multiple />
                                <input type='file' id='one'
                                    onChange={(e) => this.onOneChange(e, storage)} />
                            </Segment>
                            <Segment style={{ overflow: 'auto', maxHeight: '27em' }}>
                                <strong>response:</strong>
                                <pre> {JSON.stringify(storage.upload.response.data, null, 2)} </pre>
                            </Segment>
                        </Segment.Group>
                    </Segment>
                )}
            </MyContext.Consumer>
        )
    }
}
