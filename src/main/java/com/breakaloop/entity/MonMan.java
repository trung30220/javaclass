package com.breakaloop.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.*;

@Entity
@Getter
@Setter
@AllArgsConstructor // fix: Actual and formal arguments lists differ in length error
@NoArgsConstructor
@Slf4j
@Builder
@ToString(exclude = {"id"})
@EqualsAndHashCode(of = {"id"}, callSuper = false) // (of=User.class)
//@ApiModel(value = "MonMan")
public class MonMan extends OwnerAuditable<String> implements Serializable {
    public enum MonManType {
        INCOME,
        EXPENSE
    }

    private static final long serialVersionUID = -4253754575932590717L;

    public static final String SEPARATOR = ",";
    public static final Integer MAX_LENGTH_TAGS = 1024;
    public static final Integer MAX_LENGTH_NOTE = 1024;

    @ApiModelProperty(hidden = true)
    @GeneratedValue(strategy = GenerationType.AUTO)
    //@Setter(AccessLevel.PROTECTED)
    private @Id
    Long id;

    @Enumerated
    @NotNull
    @Column(nullable = false, columnDefinition = "smallint")
    private MonManType type;

    @NotNull
    @Column(nullable = false)
    private String note;

    @NotNull
    @Column(name = "tags", nullable = false)
    private String tags;

    @Transient
    @ApiModelProperty(hidden = true)
    @JsonIgnore
    private List<String> _tags = new ArrayList<>();

//    @JsonFormat(pattern = "MM-yyyy-dd", shape = JsonFormat.Shape.STRING, timezone="CET")
//    private LocalDate localDate;
//    @JsonFormat(pattern = "HH:mm", shape = JsonFormat.Shape.STRING, timezone="CET")
//    private LocalTime localTime;

    // return milliseconds
    //@ApiModelProperty(required = true, dataType = "org.joda.time.LocalDate", example = "1970-01-01")
    @ApiModelProperty(required = true, dataType = "java.time.LocalDate", example = "01/01/1970")
    //@JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING, timezone="CET")
    @JsonFormat(pattern = "MM/dd/yyyy", shape = JsonFormat.Shape.STRING, timezone="CET")
    private Date date;

    @NotNull
    @Column(nullable = false) // generate db with not null property
    private BigDecimal amount;

    @NotNull
    @Column(nullable = false)
    @ApiModelProperty(example = "VND")
    private String currency;

    @Transient
    @JsonIgnore
    @ApiModelProperty(hidden = true)
    private Currency _currency;

//    public Long getId() {
//        return id;
//    }

    public List<String> getTags() {
        if (tags != null && _tags.isEmpty()) {
            String[] list = tags.split(SEPARATOR);
            _tags = Arrays.asList(list);
        }
        return _tags;
    }

    public void setTags(List<String> list) {
        _tags = list;
        if (list != null) {
            tags = StringUtils.join(list, SEPARATOR);
            if (tags.length() > MAX_LENGTH_TAGS) tags = tags.substring(0, MAX_LENGTH_TAGS);
        }
    }

    public void setCurrency(String currency) {
        if (currency != null) {
            this._currency = Currency.getInstance(currency);
            this.currency = currency;
        }
    }

    public String getCurrency() {
        return this.currency;
    }

    @JsonProperty
    @ApiModelProperty(hidden = true)
    public void setCurrencyCode(Currency currency) {
        this._currency = currency;
        this.currency = currency.getCurrencyCode();
    }

    @JsonIgnore
    @ApiModelProperty(hidden = true)
    public Currency getCurrencyCode() {
        if (_currency == null && currency != null) {
            _currency = Currency.getInstance(currency);
        }
        return _currency;
    }

    public void setNote(String note) {
        if (note != null && note.length() > MAX_LENGTH_NOTE) this.note = note.substring(0, MAX_LENGTH_NOTE);
        else this.note = note;
    }
}
